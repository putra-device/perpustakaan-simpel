import Topbar from '@/components/layouts/Topbar'
import { Html, Head, Main, NextScript } from 'next/document'

export default function Document() {
  return (
    <Html lang="en">
      <Head />
      <body>
        <div className='wrapper'>
        </div>
        <Main />
        <NextScript />
      </body>
    </Html>
  )
}
